# pyfindfile

Fast utility for searching your entire file system for files and folders based on a file name using a fuzzy search.
## About
pyfindfile uses substring matching to find files and folders in a starting directory. It traverses your file system and looks for any matches it can find. 

This is useful if you are looking for where you left a file but you know its name. Searching a file's contents is slower and more complex, and this tool **does not do this**. 
## Running
1. Download Python  (3.8+ ideally) from [the official website](https://python.org/).
2. Download the program [here](https://codeberg.org/WinFan3672/pyfindfile/raw/branch/main/pyfindfile.py).
3. Run it using python. Double-clicking it won't work, so you need to open a terminal, or command prompt if you're on Windows.#

## Contributing
I don't intend to update the tool much, after all, it is meant to be basic. However, feel free to create an issue or pull request if you find/add anything noteworthy.