#!/usr/bin/env python
import os
from fuzzysearch import find_near_matches

import os
from fuzzysearch import find_near_matches

def search(top, term):
    total_files, total_dirs = [], []
    for root, dirs, files in os.walk(top):
        print("Trying {}".format(root))
        dir_matching_items = [dir_name for dir_name in dirs if term in dir_name.lower()]
        file_matching_items = [file_name for file_name in files if term in file_name.lower()]

        total_files += [os.path.join(root, x) for x in file_matching_items]
        total_dirs += [os.path.join(root, x) for x in dir_matching_items]

    print("Listing All Matches:")
    for item in total_dirs + total_files:
        print(item)


def main():
    print("Welcome to pyFileFinder.")
    top = ""
    while not top:
        top = input("Starting Directory $")
        if not top:
            print("ERROR: This is required.")
    term = ""
    while not term:
        term = input("File/folder name $").lower()
        if not term:
            print("ERROR: This is required.")
    search(top, term)
main()
